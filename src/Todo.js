import React , {useContext, memo } from "react";
import useToggle from './hooks/useToggle';

import EditTodo from './EditTodo';

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CheckBox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";

import { DispatchContext } from './context/todos.context';

function Todo({ id, task, completed }) {

  console.log(task);

  const  dispatch  = useContext(DispatchContext);

  const [editModeTrue, setEditModeTrue] = useToggle(false);
  

  return (

    
    
    <ListItem style={{height: '64px', marginBottom: '5px'}}>

      {editModeTrue ? <EditTodo id={id} task={task} editing={setEditModeTrue}/> : 
    <>
      <CheckBox tabIndex={-1} checked={completed} onClick={() => dispatch({type: "TOGGLE", id: id})}/>
      <ListItemText
        style={{ textDecoration: completed ? "line-through" : "none" }}
      >
        {task}
      </ListItemText>

      <ListItemSecondaryAction>
        <IconButton aria-label="Edit Task" onClick={setEditModeTrue}>
          <EditIcon />
        </IconButton>
        <IconButton aria-label="Delete Task" onClick={() => dispatch({type: "DELETE", id: id})}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </>
    }
      
    </ListItem>
    
  );
}

export default memo(Todo);
