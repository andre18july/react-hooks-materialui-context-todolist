import React , { createContext , useReducer} from 'react';
import todoReducer from '../reducers/todo.reducer';
import uuid from "uuid";

const defaultTodos = [
    { id: uuid(), task: "Going ride bike", completed: false },
    { id: uuid(), task: "Walk in the jungle", completed: true },
    { id: uuid(), task: "Take a snack", completed: false }
];

export const TodosContext = createContext();
export const DispatchContext = createContext();

export function TodosProvider(props) {  

    const [todos, dispatch] = useReducer(todoReducer, defaultTodos);
    return(
        <TodosContext.Provider value={todos}>
            <DispatchContext.Provider value={dispatch}>
                {props.children}
            </DispatchContext.Provider>
        </TodosContext.Provider>
    )
}