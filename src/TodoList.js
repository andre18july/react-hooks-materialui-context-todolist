import React , { useContext } from "react";
import Todo from "./Todo";
import { TodosContext } from './context/todos.context';
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";

function TodoList() {


  const todos = useContext(TodosContext);

  return (
    <Paper>
      <List>
        {todos.map((todo, index) => (
          <React.Fragment key={index}>
            <Todo {...todo} key={todo.id}/>
            {index < todos.length - 1 && <Divider />}
          </React.Fragment>
        ))}
      </List>
    </Paper>
  );
}

export default TodoList;
