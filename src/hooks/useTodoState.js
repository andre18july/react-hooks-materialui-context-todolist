import uuid from "uuid";
import useLocalStorage from './useLocalStorage';

function useTodoState(initialTodos) {


  const [todos, setTodos] = useLocalStorage("todos", initialTodos);

  return {
    todos,
    addTask: task => {
      const oldTodos = todos.map(todo => ({ ...todo }));
      setTodos([...oldTodos, { id: uuid(), task: task, completed: false }]);
    },
    deleteTask: id => {
      //pesquisar na estrutura de dados pela task com o mesmo id
      const oldTodos = todos.map(todo => ({ ...todo }));
      const updatedTodos = oldTodos.filter(todo => todo.id !== id);
      setTodos(updatedTodos);
    },
    toggleTask: id => {
      const oldTodos = todos.map(todo => ({ ...todo }));
      const updatedTodos = oldTodos.map(todo =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      );
      setTodos(updatedTodos);
    },
    editTodo: (id, task) => {
      const oldTodos = todos.map(todo => ({ ...todo }));
      const updatedTodos = oldTodos.map(todo =>
        todo.id === id ? { ...todo, task: task } : todo
      );
      setTodos(updatedTodos);
    }
  };
}

export default useTodoState;
