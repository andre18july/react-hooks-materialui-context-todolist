import { useState } from 'react';

function useFormState(initialVal){

    const [input, setInput] = useState(initialVal);

    const changeInput = e => {
        setInput(e.target.value);
    }

    const resetInput = () => {
        setInput("");
    }

    return [input, changeInput, resetInput];
}

export default useFormState;