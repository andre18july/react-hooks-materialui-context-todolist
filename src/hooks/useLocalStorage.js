import { useState, useEffect } from 'react';


function useLocalStorage(initialKey, initialValue){


    const [state, setState] = useState(() => {
        let val;
        try{
            val = JSON.parse(window.localStorage.getItem(initialKey)) || initialValue;
        }catch(e){
            val = initialValue;
        }

        return val;
    });


    useEffect(() => {
        window.localStorage.setItem(initialKey, JSON.stringify(state));
    }, [state])
    

    return [state, setState];
}

export default useLocalStorage;