import React , { useContext } from "react";
import useFormState from "./hooks/useFormState";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { DispatchContext } from './context/todos.context';


function TodoForm() {


  const [task, setTask, resetTask] = useFormState("");
  const dispatch = useContext(DispatchContext);

  const handleClick = e => {
    e.preventDefault();
    dispatch({type: "ADD", task: task })

    resetTask();
  };


  return (
    <Paper style={{margin: "1rem 0", padding: "10px 30px"}}>
      <form onSubmit={handleClick}>
        <TextField
          label="Task"
          margin="normal"
          type="text"
          value={task}
          onChange={setTask}
          fullWidth
        />
      </form>
    </Paper>
  );
}

export default TodoForm;
