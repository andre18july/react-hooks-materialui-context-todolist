import React, { useContext } from "react";
import useFormState from "./hooks/useFormState";
import { DispatchContext } from './context/todos.context';
import TextField from "@material-ui/core/TextField";

function EditTodo({ todo, editing }) {


  const dispatch = useContext(DispatchContext);
  const [updatedTask, setUpdatedTask, reset] = useFormState(todo.task);

  const handleSubmit = e => {
    e.preventDefault();
    dispatch({type: "EDIT", id: todo.id, task: updatedTask});
    reset();
    editing(false);
  };

  return (
 
      <form onSubmit={handleSubmit} style={{width: "100%" , marginRight: "20px", padding: "0px"}}>
        <TextField
          label="Task Edit"
          margin="normal"
          type="text"
          value={updatedTask}
          onChange={setUpdatedTask}
          fullWidth
          autoFocus
          style={{marginLeft: "10px", padding: "0px"}}
        />
      </form>

  );
}

export default EditTodo;
