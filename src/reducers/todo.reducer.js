import uuid from 'uuid';

const reducer = (state, action) => {
    switch(action.type){
        case "ADD": {
            const oldTodos = state.map(todo => ({ ...todo }));
            return [...oldTodos, { id: uuid(), task: action.task, completed: false }];
        }
        case "DELETE": {
            const oldTodos = state.map(todo => ({ ...todo }));
            const updatedTodos = oldTodos.filter(todo => todo.id !== action.id);
            return updatedTodos;
        }
        case "TOGGLE": {
            const oldTodos = state.map(todo => ({ ...todo }));
            const updatedTodos = oldTodos.map(todo =>
              todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
            );
            return updatedTodos;
        }
        case "EDIT": {
            const oldTodos = state.map(todo => ({ ...todo }));
            const updatedTodos = oldTodos.map(todo =>
            todo.id === action.id ? { ...todo, task: action.task } : todo
            );
            return updatedTodos;
        }
        default:
            return state;
    }
}


export default reducer;